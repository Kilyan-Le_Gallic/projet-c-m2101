//Recupere une sous-chaine d'une chaine ch de l'indice debut+orign jusqu'a n
char * retrievesubstr(char *ch,const char *orign,int debut, int n){
   strncpy(ch,(orign+debut),n);
   ch[n]='\0';//Neutralisation de la fin de chaine
   return ch;
}

//Indique si la chaine ch passee en parametre est une trame GPGGA
//Retourne 1 si ch est une trame GPGGA
//Leve PAS_GPGGA si ch n'est pas une trame GPGGA
int estGPGGA(char ch[],jmp_buf ptRep){
   if(strstr(ch,"$GPGGA")!=NULL && strlen(ch)>15){//Si il y le motif "GPGGA" dans la trame et plus de 15 chars, OK
      return 1;
   } else {
      longjmp(ptRep, PAS_GPGGA);
      return 0;
   }
}

//Convertit une chaine de caracteres ch en float p, et transforme p en degre
//minute et secondes
//Retourne un pointeur char
//Leve PTR_NULL si ch==NULL
char * convertirLonLat(char * ch, jmp_buf ptRep){
   if(ch==NULL){
      longjmp(ptRep,PTR_NULL);
   }
   double p = atof(ch); //Transformation de la chaine de caract�res en double
   double minutet; //Variable de transition
   int degre;
   int minute;
   double secon;
   p=p/100;
   degre = ((int)floor(p));
   minutet = (p-degre)*60;
   minute=(int)floor(minutet);
   secon =(minutet-minute)*60;
   char * str = calloc(14,sizeof(char));
   sprintf(str,"%d°%d'%.*f''",degre,minute,2,secon);
   return str;
}

//Formate une chaine str construite a� partir des champs d'une structure
//Temps, au format XXhYYmZZs
//Leve PTR_NULL si tem==NULL
char * formaterHeure(Temps * tem, jmp_buf ptRep){
   if(tem==NULL){
      longjmp(ptRep,PTR_NULL);
   }
   char * str = calloc(14,sizeof(char));
   sprintf(str,"%sh%sm%ss",tem->heure,tem->minute,tem->secondes);//Mise
   return str;

}



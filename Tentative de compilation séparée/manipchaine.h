//Recupere une sous-chaine d'une chaine ch de l'indice debut+orign jusqu'a n
char * retrievesubstr(char *ch,const char *orign,int debut, int n);

//Indique si la chaine ch passee en parametre est une trame GPGGA
//Retourne 1 si ch est une trame GPGGA
//Leve PAS_GPGGA si ch n'est pas une trame GPGGA
int estGPGGA(char ch[],jmp_buf ptRep);
 

//Formate une chaine str construite a� partir des champs d'une structure
//Temps, au format XXhYYmZZs
//Leve PTR_NULL si tem==NULL
char * formaterHeure(Temps * tem, jmp_buf ptRep);





